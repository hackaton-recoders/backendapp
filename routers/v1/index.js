const loginRouter = require('../v1/login_router');
const UsuarioRouter = require('../v1/login_router');
const ArticuloRouter = require('../v1/articulo_router');


module.exports = (app) =>{

    app.use('/api/v1', loginRouter);
    app.use('/api/v1', UsuarioRouter);
    app.use('/api/v1', ArticuloRouter);
}