//modulo de terceros
const express = require('express');

//modulo local
const { signup } = require('../../controller/v1/login_controller');
const { validateSingup } = require('../../validator/vlogin');

const router = express.Router();

//router.post('/login', login);
router.post('/signup', validateSingup, signup); //crearse una cuenta


module.exports = router;