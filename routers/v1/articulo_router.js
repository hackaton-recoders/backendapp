const express = require('express');

const { 
    listar,
    actualizar,
    borrar,
    getArticuloId,
    registrar
} = require('../../controller/v1/articulo_controller');

const router = express.Router();

    router.get('/articulo', listar);
    router.get('/articulo/:id', getArticuloId);
    router.put('/articulo/:id', actualizar);
    router.post('/articulo', registrar);
    router.delete('/articulo/:id', borrar);

module.exports = router;