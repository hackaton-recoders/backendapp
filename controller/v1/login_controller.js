const ModelUsuario = require('../../models/model_usuario');


//capturar errores
function errorHnadler(err, next, data) {
    if(err){
        return next(err);
    }
    if(!data){
        const err = new Error('No existe');
        err.statusCode = 500;
        return next(err);
    }
}



// SignUp ->Crear un usuario

const signup = (req, res, next) =>{

   /*
   let item = {
        nombre: req.body.nombre,
        correo: req.body.correo,
        password: req.body.password,
        role: req.body.role

    } */ 

    let modelUsuario = new ModelUsuario(req.body);
 
    modelUsuario.save((err, data) =>{
        if(err || !data) return errorHnadler(err, next, data);
 
        res.json({
            result: true,
            usuario: data
        })
    })
 }

 module.exports = {
    signup
 }