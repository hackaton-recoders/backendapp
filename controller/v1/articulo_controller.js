const ModelArticulo = require('../../models/model_articulo');

//capturar errores
function errorHnadler(err, next, data) {
    if(err){
        return next(err);
    }
    if(!data){
        const err = new Error('No existe');
        err.statusCode = 500;
        return next(err);
    }
}


//Listar articulos

const listar = (req, res) =>{

    res.json({
        ok: true,
        articulos:data
    })
}

//listar articulos por ID

const getArticuloId = async (req, res) =>{

    let articuloId = req.params.id;

    articuloId = await ModelArticulo.findById(articuloId).exec();

    res.json({
        result: true,
        articulo: articuloId
    })

}

//Resgistrar usuario

const registrar = (req, res, next) =>{

   let articuloModelo = new ModelArticulo(req.body);

   articuloModelo.save((err, data) =>{
       if(err || !data) return errorHnadler(err, next, data);

       res.json({
           result: true,
           articulo: data
       })
   })
}

//Actualizar articulo

const actualizar = (req, res) =>{

    res.json({
        ok: true,
        articulos:data[0]
    })
}

//Borrar articulo

const borrar = (req, res) =>{

    res.json({
        ok: true,
        articulos:data[0]
    })
}
module.exports = {
    listar,
    actualizar,
    borrar,
    registrar,
    getArticuloId
}