var mongoose = require('mongoose');


var Schema = mongoose.Schema;

//Creación del esquema de la entidad Producto

var articuloSchema = new Schema({

    nombre: { type: String, required:true},
    descripcion: {type: String, required:true,},
    aplausos:{type: Number},
    imagen: { type: String},
   

}, {timestamps: true}, {collections: 'Articulos'});

module.exports = mongoose.model('Articulo', articuloSchema);