var mongoose = require('mongoose');


var Schema = mongoose.Schema;

//Creación del esquema de la entidad Producto

var donacionSchema = new Schema({

    nombre: { type: String, required:true},
    modalidad:{type: String, default:'UNICA'},
    correo: {type: String, required:true,},
    telefono:{type: String, required:true},
    monto: { type: Number, required:true},
   

}, {timestamps: true}, {collections: 'Donaciones'});

module.exports = mongoose.model('Donacion', donacionSchema);