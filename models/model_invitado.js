var mongoose = require('mongoose');


var Schema = mongoose.Schema;

//Creación del esquema de la entidad Producto

var usuarioSchema = new Schema({

    nombre: { type: String, required:true},
    correo: {type: String, required:true, unique: true},
    opcion_donar: { type: Boolean, default:false}, // false: donar, true: conocer
    isPersonaPrivada: {type: Boolean, default:true}, //true: persona comun, false: institucion
    cant_vinculos: {type: Number, required: true},
    role:{ type: String, default: 'INVITADO_ROLE'}

}, {timestamps: true}, {collections: 'Usuarios'});

module.exports = mongoose.model('Usuario', usuarioSchema);