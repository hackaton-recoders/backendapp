var mongoose = require('mongoose');


var Schema = mongoose.Schema;

//Creación del esquema de la entidad Producto

var usuarioSchema = new Schema({

    nombre: { type: String, required:true},
    //edad: { type: Number, required:[true, 'La edad debe es requerido']},
    correo: {type: String, required:true, unique: true},
    password: { type: String, required:true},
    enfermedad: {type: String},
    descripcion: { type: String},
    cant_vinculos: {type: Number },
    role:{ type: String, default: 'USER_ROLE'}

}, {timestamps: true}, {collections: 'Usuarios'});

module.exports = mongoose.model('Usuario', usuarioSchema);