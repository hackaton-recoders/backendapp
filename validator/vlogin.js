const { body, validationResult } = require('express-validator');
const ModelUsuario = require('../models/model_usuario');

const pSignup = [
    body("correo")
        .isEmail()
        .withMessage("Ingrese un correo válido")
        .custom((value) =>{
            //console.log(value);
            return ModelUsuario.findOne({correo: value }).then( userDoc=>{
                if(userDoc){
                    return Promise.reject('Este correo ya existe: validator')
                }
            });
        }),
    body("nombre").trim()
        .not()
        .isEmpty(),
    body("password").trim()
        .isLength({min:5})
        .withMessage("Minimo 5 caracteres"),

]

   const vSignup = (req, res, next) => {
       const errors = validationResult(req);
      // console.log(errors);
        
       if(!errors.isEmpty()){
           const error = new Error('Error validacion');
           error.statusCode = 200;
           error.data = errors.array();
           return next(error);
       }

       next();
   }

   const validateSingup = [pSignup,vSignup];

   module.exports = {
       validateSingup
   }